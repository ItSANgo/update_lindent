#! /usr/bin/make -f

prefix=$(HOME)
bin_dir=$(prefix)/bin
repository_dir=$(HOME)/src/repos/kernel.org/torvalds/linux

script_dir=scripts
TARGET=Lindent

.PHONY: all clean install uninstall
all:
	cd $(repository_dir) && git pull
	cp -p $(repository_dir)/$(script_dir)/$(TARGET) .
clean:
	$(RM) $(TARGET)
install:
	install $(TARGET) $(bin_dir)
uninstall:
	cd $(bin_dir) && $(RM) $(TARGET)
